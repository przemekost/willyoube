package com.ostrouchprzemyslaw.willyoube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WillYouBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WillYouBeApplication.class, args);
    }

}
